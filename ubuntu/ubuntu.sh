#!/bin/bash

echo Actualizacion paquetes y actualizando cada paquete:

#Actualizacion del Sistema
sudo apt update
sudo apt upgrade

echo  Removiendo depedencias obsoletas:

# Elimina todas las dependencias que ya no necesita el sistema
sudo apt autoremove

#Remover paquetes de snap
echo Removiendo paquetes:
sudo snap remove snap-store 
sudo apt purge firefox gnome-terminal

echo Instalación de varios paquetes:

#Instalacionde programas o terminales
sudo apt install -y curl wget git tilix gnome-boxes vim neovim



